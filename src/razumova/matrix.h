#pragma once

#include <iostream>

class Matrix
{
public:
	Matrix();
	Matrix(int n, int m);
	Matrix(const Matrix& m);

	double*& operator[](int index);
	double*& operator[](int index) const;
	double& operator()(int i, int j);
	double& operator()(int i, int j) const;

	bool operator==(const Matrix& m) const;
	bool operator!=(const Matrix& m) const;

	Matrix operator+(const Matrix& m);
	Matrix operator-(const Matrix& m);
	Matrix operator* (const Matrix& m);
	Matrix operator* (const double& d);
	Matrix operator/ (const double& d);

	Matrix& operator=(const Matrix &m);
	Matrix& operator+=(const Matrix &m);
	Matrix& operator-=(const Matrix& m);
	Matrix& operator*=(const double& d);
	Matrix& operator/=(const double& d);

	Matrix& fillRand();

	friend std::ostream& operator<<(std::ostream& stream, const Matrix& m);
	friend std::istream& operator>> (std::istream& stream, Matrix& m);

	~Matrix();
private:
	int lines, collumns;
	double **matr;
};

std::ostream& operator<<(std::ostream& stream, const Matrix& m);
std::istream& operator>> (std::istream& stream, Matrix& m);
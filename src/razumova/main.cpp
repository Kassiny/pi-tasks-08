#include "matrix.h"
#include <fstream>
#include <time.h>

using namespace std;

int main()
{
	srand(time(0));
	ifstream file("in.txt");
	ofstream out("out.txt");
	Matrix a(3, 3), b(3, 3), c(3, 3);
	file >> a >> b;
	out << a  << b;
	out << "a+b" << endl << a + b << endl << "a-b" << endl << a - b << "a*b" << endl << a*b << endl;
	a *= 5;
	b /= 2;

	out << "a*=5" << endl << a << endl << "b/=2" << endl << b << endl;

	Matrix d(3, 3);
	file >> d;
	d += a;
	out << "zero 3x3 matrix d+=a" << endl << d;
	d -= b;
	out << endl << "d-=b" << endl << d << endl;
	if (a == b)
		out << "a=b" << endl;
	else if (a!=b)
		out << "a!=b" << endl;
	return 0;
}
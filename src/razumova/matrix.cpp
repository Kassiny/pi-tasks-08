#include "matrix.h"

Matrix::Matrix():lines(2),collumns(2)
{
	matr = new double* [2];
	for (size_t i = 0; i < 2; i++)
	{
		matr[i] = new double[2];
	}
}

Matrix::Matrix(int m, int n):lines(m),collumns(n)
{
	matr = new double*[m];
	for (size_t i = 0; i < m; i++)
	{
		matr[i] = new double[n];
	}
}

Matrix::Matrix(const Matrix & m)
{
	lines = m.lines;
	collumns = m.collumns;
	matr = new double*[lines];
	for (size_t i = 0; i < lines; i++)
	{
		matr[i] = new double[collumns];
	}
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
		{
			matr[i][j] = m[i][j];
		}
	}

}

double*& Matrix::operator[](int index)
{
	return matr[index];
}

double*&  Matrix::operator[](int index) const
{
	return matr[index];
}

double & Matrix::operator()(int i, int j)
{
	if (i < 0 || i >= lines || j < 0 || j >= collumns)
		throw "Out of range";
	return matr[i][j];
}

double & Matrix::operator()(int i, int j) const
{
	if (i < 0 || i >= lines || j < 0 || j >= collumns)
		throw "Out of range";
	return matr[i][j];
}

bool Matrix::operator==(const Matrix & m) const
{
	if (lines != m.lines || collumns != m.collumns)
		return false;
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
			if (matr[i][j] != m[i][j])
				return false;
	}
	return true;
}

bool Matrix::operator!=(const Matrix & m) const
{
	return !(*this == m);
}

Matrix Matrix::operator+(const Matrix & m)
{
	if (lines != m.lines || collumns != m.collumns)
		throw 1;
	Matrix tmp(lines, collumns);
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
		{
			tmp(i, j) = matr[i][j] + m[i][j];
		}
	}
	return tmp;
}

Matrix Matrix::operator-(const Matrix & m)
{

	if (lines != m.lines || collumns != m.collumns)
		throw 1;
	Matrix tmp(lines, collumns);
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
		{
			tmp(i,j) = matr[i][j] - m[i][j];
		}
	}
	return tmp;
}

Matrix Matrix::operator*(const Matrix & m)
{
	if (collumns != m.lines)
		throw "different sizes multiply";
	Matrix tmp(lines, m.collumns);
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < m.collumns; j++)
		{
			tmp(i, j) = 0;
			for (size_t k = 0; k < collumns; k++)
			{
				tmp.matr[i][j] += (matr[i][k] * m.matr[k][j]);
			}
		}
	}
	return tmp;
}

Matrix Matrix::operator*(const double & d)
{
	Matrix tmp(lines, collumns);
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
		{
			tmp(i, j) = matr[i][j] * d;
		}
	}
	return tmp;
}

Matrix Matrix::operator/(const double & d)
{
	if (d == 0)
		throw "divide by zero";
	Matrix tmp(lines, collumns);
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
		{
			tmp(i, j) = matr[i][j] / d;
		}
	}
	return tmp;
}

Matrix & Matrix::operator=(const Matrix & m)
{
	if (&m == this)
	{
		return *this;
	}
	if (m.collumns != collumns || m.lines != lines)
	{
		throw 1;
	}
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
		{
			matr[i][j] = m[i][j];
		}
	}
	return *this;
}

Matrix & Matrix::operator+=(const Matrix & m)
{
	if (lines != m.lines || collumns != m.collumns)
		throw "different sizes";
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
			matr[i][j] += m[i][j];
	}
	return *this;
}

Matrix & Matrix::operator-=(const Matrix & m)
{
	if (lines != m.lines || collumns != m.collumns)
		throw "different sizes";
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
			matr[i][j] -= m[i][j];
	}
	return *this;
}

Matrix & Matrix::operator*=(const double & d)
{
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
			matr[i][j] *= d;
	}
	return *this;
}

Matrix & Matrix::operator/=(const double & d)
{
	if (d == 0)
		throw "divide by zero";
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
			matr[i][j] /= d;
	}
	return *this;
}

Matrix& Matrix::fillRand()
{
	for (size_t i = 0; i < lines; i++)
	{
		for (size_t j = 0; j < collumns; j++)
		{
			matr[i][j] = rand() % 100;
		}
	}
	return *this;
}

Matrix::~Matrix()
{
	for (size_t i = 0; i < lines; i++)
	{
		delete[] matr[i];
	}
	delete[] matr;
}

std::ostream & operator<<(std::ostream & stream, const Matrix & m)
{
	for (size_t i = 0; i < m.collumns; i++)
	{
		for (size_t j = 0; j < m.lines; j++)
		{
			stream << m(i, j) << " ";
		}
		stream << std::endl;
	}
	return stream;
}

std::istream & operator >> (std::istream & stream, Matrix & m)
{
	for (size_t i = 0; i < m.collumns; i++)
	{
		for (size_t j = 0; j < m.lines; j++)
		{
			stream >> m(i, j);
		}
	}
	return stream;
}
